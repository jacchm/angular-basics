import {EventEmitter, Injectable} from '@angular/core';
import {Book} from './model/Book';

// import {Subject} from 'rxjs';

export interface DataServiceInterface {
  books: Array<Book>;
  bookAddedEvent: EventEmitter<Book>;
  bookDeletedEvent: EventEmitter<Book>;

  addBook(book: Book): void;

  deleteLastBook(): void;
}


@Injectable({
  providedIn: 'root'
})
export class DataService implements DataServiceInterface{

  books: Array<Book>;
  bookAddedEvent = new EventEmitter<Book>();
  bookDeletedEvent = new EventEmitter<Book>();

  // bookAddedEvent2 = new Subject<Book>();


  constructor() {
    this.books = new Array<Book>();
    const book1 = new Book();
    book1.title = 'first book';
    book1.author = 'matt';
    book1.price = 3.99;
    this.books.push(book1);

    const book2 = new Book();
    book2.title = 'second book';
    book2.author = 'james';
    book2.price = 5.99;
    this.books.push(book2);

    const book3 = new Book();
    book3.title = 'thrid book';
    book3.author = 'laura';
    book3.price = 8.99;
    this.books.push(book3);
  }

  addBook(book: Book): void {
    if (book.author === 'james') {
      this.bookAddedEvent.error('Books by james are not allowed');
    } else {
      this.books.push(book);
      this.bookAddedEvent.emit(book);
    }
  }

  deleteLastBook(): void {
    if (this.books.length > 0) {
      this.bookDeletedEvent.emit(this.books.pop());
    } else {
      this.bookDeletedEvent.error('There is no book left to be deleted.');
    }
  }


}
