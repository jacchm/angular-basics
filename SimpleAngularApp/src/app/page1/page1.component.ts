import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {DataServiceInterface} from '../data.service';
import {Book} from '../model/Book';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-page1',
  templateUrl: './page1.component.html',
  styleUrls: ['./page1.component.css']
})
export class Page1Component implements OnInit, OnDestroy {

  pageName = 'Page 1';
  books: Array<Book>;
  numberOfBooksWrittenByMatt: number;

  subscription: Subscription;
  deleteSubscription: Subscription;

  constructor(@Inject('DataServiceInterface')private dataService: DataServiceInterface) {
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.pageName = 'First Page';
    }, 5000);
    this.books = this.dataService.books;
    this.numberOfBooksWrittenByMatt = this.books.filter(it => it.author === 'matt').length;
    this.subscription = this.dataService.bookAddedEvent.subscribe(
      (newBook) => {
        if (newBook.author === 'matt') {
          this.numberOfBooksWrittenByMatt++;
        }
      },
      (error) => {
        console.log('an error occured', error);
      },
      () => {
        // complete event
      }
    );

    this.deleteSubscription = this.dataService.bookDeletedEvent.subscribe(
      (deletedBook) => {
        if (deletedBook.author === 'matt') {
          this.numberOfBooksWrittenByMatt--;
        }
      }
    );
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.deleteSubscription.unsubscribe();
  }


  onButtonClick(): void {
    alert('hello from allert - the date today is ' + new Date());
  }

}
