import {Component, OnDestroy, OnInit} from '@angular/core';
import {DataService} from '../data.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-page3',
  templateUrl: './page3.component.html',
  styleUrls: ['./page3.component.css']
})
export class Page3Component implements OnInit, OnDestroy {

  constructor(private dataService: DataService) {
  }

  subscription: Subscription;

  ngOnInit(): void {
    this.subscription = this.dataService.bookDeletedEvent.subscribe(
      (deletedBook) => {
        if (deletedBook != null) {
          console.log('Item has been deleted');
        }
      },
      (error) => {
        console.log('An error occurred. ', error);
      }
    );
  }

  deleteLastBook(): void {
    this.dataService.deleteLastBook();
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
