export enum SubjectArea {
  ART = 'Arts and crafts',
  HISTORY = 'History',
  SCIENCE = 'Science and Maths',
  LITERATURE = 'Classic literature'
}

export enum SthToTestEnum {
  DOOM, HEROES, WTF, SOMETHING
}


export class Book {
  title: string;
  author: string;
  price: number;
  readonly id: number = 1;

  constructor(author: string, title?: string, price?: number) {
    this.author = author;
    if (title) {
      this.title = title;
    }
    if (price) {
      this.price = price;
    }
  }

  toString(): string {
    return `title ${this.title}, author ${this.author}`;
  }

  priceWtihTax(taxRate: number): number {
    return this.price * (1 + taxRate);
  }

}

export class Video {
  title: string;
  author: string;
  price: number;
}
