import {Component} from '@angular/core';
import {Book, SthToTestEnum, SubjectArea, Video} from '../model/Model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor() {


    console.log('-------INDICES AND VALUES------');
    // this shows indices and values
    for (const sth in SthToTestEnum) {
      console.log(sth);
    }

    // this shows indices and values
    const enumArr = Object.keys(SthToTestEnum);
    for (const sth of enumArr) {
      console.log(sth);
    }

    console.log('-----------VALUES--------------');
    // this shows values
    for (const sth of enumArr.slice(enumArr.length / 2)) {
      console.log(sth);
    }


    console.log('-------------------------------');

    console.log(SubjectArea.HISTORY);
    console.log(SubjectArea.LITERATURE);
    console.log(SubjectArea['Science and Maths']);

    for (const subject in SubjectArea) {
      console.log(subject);
      console.log(SubjectArea[subject]);
    }

    // only needed for enums without custom values
    const enumArray = Object.keys(SubjectArea);
    for (const subject of enumArray.slice(enumArray.length / 2)) {
      console.log(subject);
      console.log(SubjectArea[subject]);
    }

    let label;
    for (const subject in SubjectArea) {
      if (SubjectArea[subject] === 'Science and Maths') {
        label = subject;
      }
    }
    console.log(`The matching label is ${label}`);

    let label2 = Object.keys(SubjectArea).find(it => SubjectArea[it] === 'Science and Maths');

    console.log(label2);

    // this.exploringArrays();

    this.objectEquality();

    let myCustomer = {firstName: 'Matt', age: 21};
    console.log(myCustomer);
    console.log(typeof myCustomer);

    let myBook = new Book('Matt');
    let myVideo: Video;

    myBook.title = 'A fantastic read';
    console.log(`My book is`, myBook);
    myBook.price = 100;
    console.log(`to buy this book it will cost ${myBook.priceWtihTax(0.2)}`);

    const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    const oddNumbers = numbers.filter(
      num => {
        return num % 2 === 1;
      }
    );
    const evenNumbers = numbers.filter(num => num % 2 === 0);

    console.log(oddNumbers);
    console.log(evenNumbers);
  }

  objectEquality(): void {
    let myValue: number;

    console.log(1 == 1);
    console.log(1 === 1);
    console.log('1' == 1);
    console.log('1' === 1);
    console.log(myValue == null);
    console.log(myValue == undefined);
  }

  title = 'ExploringTypescript';

  myNumer: number;
  readonly myNumber: number = 3;

  // tslint:disable-next-line:typedef
  someMethod() {
    let anotherNumber = 0;
    anotherNumber = 1;
    const nextNumber = 10;

    console.log(this.myNumber);
  }

  // tslint:disable-next-line:typedef
  exploringArrays() {
    const myArray1 = new Array<number>(5);
    const myArray2: number[] = [1, 2, 3, null];
    console.log(myArray1);
    console.log(myArray2);
    console.log(myArray2[1]);
    console.log(myArray2.slice(0, 2));
    console.log(myArray2.splice(1, 1));
    console.log(myArray2);
    myArray2.push(4);
    myArray2.push(5);
    console.log('My array2:' + myArray2);
    console.log(myArray2.pop());
    console.log(myArray2);

    for (let i = 0; i < 10; i++) {
      console.log(i);
    }

    // of is for the values
    for (const next of myArray2) {
      console.log(next);
    }

    // in is for the indices
    for (const next in myArray2) {
      console.log(next);
    }

    let num = 0;
    while (num < 5) {
      console.log(num);
      num++;
    }

    do {
      console.log(num);
      num++;
    } while (num < 10);

  }


}
