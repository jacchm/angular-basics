export class Book {
  title: string;
  author: string;
  price: number;
  readonly id: number = 1;

  constructor(author: string, title?: string) {
    this.author = author;
    if (title) {
      this.title = title;
    }
  }

  // toString() {
  //   return `title ${this.title}, author ${this.author} `;
  // }

  priceWithTax(taxRate: number) :number {
    return this.price * (1+taxRate);
  }

}

export class Video {
  title: string;
  author: string;
  price: number;
}
