import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ExploringTypescript';

  readonly myNumber: number = 3;

  constructor() {
    this.exploringArrays();
  }

  someMethod() {
    let anotherNumber = 0;
    const aThirdNumber = 1;

    anotherNumber = 1;
    // anotherNumber = 'hello';

    console.log(this.myNumber);
    // this.myNumber = 8;
  }

  exploringArrays() {
    const myArray1 = new Array<number>();
    const myArray2 : number[] =[1,2,3,null];
    console.log(myArray1);
    console.log(myArray2);
    console.log(myArray2[1]);
    console.log(myArray2.splice(1,1));
    console.log(myArray2[1]);
    myArray2.push(4);
    myArray2.push(5);
    console.log(myArray2);
    console.log(myArray2.pop());
    console.log(myArray2);

    for (let i = 0; i < 10; i++) {
      console.log(i);
    }

    for (const next of myArray2) {
      console.log(next);
    }

    let num = 0;
    while (num < 5) {
      console.log(num);
      num++;
    }

    if (num < 5) {
      console.log('the number is less than 5');
    }
    else {
      console.log('the number is 5 or greater');
    }
  }
}
