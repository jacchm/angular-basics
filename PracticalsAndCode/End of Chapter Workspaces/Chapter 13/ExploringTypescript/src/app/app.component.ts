import { Component } from '@angular/core';
import {Book, SubjectArea, Video} from "../model/Model";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ExploringTypescript';

  readonly myNumber: number = 3;

  constructor() {

    console.log(SubjectArea.HISTORY);
    console.log(SubjectArea['Science and Maths']);

    for(const subject in SubjectArea) {
      console.log(subject);
      console.log(SubjectArea[subject]);
    }

    // Only needed for enums without custom values
    const enumArray = Object.keys(SubjectArea);
    for (const value of enumArray.slice(enumArray.length / 2)) {
      console.log(value);
    }

    let label;
    for(const subject in SubjectArea) {
      if (SubjectArea[subject] === 'Science and Maths') {
        label = subject;
      }
    }

    let label2 = Object.keys(SubjectArea).find( it => SubjectArea[it] === 'Science and Maths');

    console.log(`The matching label is ${label}`);
    console.log(`The matching label is ${label2}`);

    // this.exploringArrays();

    // this.objectEquality();
    //
    // let myCustomer = {firstName : 'Matt', age: 21};
    // console.log(myCustomer);
    // console.log(typeof myCustomer);
    //
    // let myBook = new Book('Matt');
    // let myVideo : Video;
    //
    // myBook.title = 'A fantastic read';
    // console.log('My book is ' + myBook);
    // console.log('My book is ' , myBook);
    // myBook.price = 100;
    // console.log(`to buy this book it will cost ${myBook.priceWithTax(0.2)} `);
    //
    // const numbers = [1,2,3,4,5,6,7,8,9,10];
    // const oddNumbers = numbers.filter(
    //   (num) => {
    //     return num % 2 === 1;
    //   }
    // );
    // const evenNumbers = numbers.filter( num => num % 2 === 0);
    // console.log(oddNumbers);
    // console.log(evenNumbers);

  }

  objectEquality() {
    let myValue: number;
    console.log(1 == 1);
    console.log(1 === 1);
//    console.log('1' == 1);
//    console.log('1' === 1);
    console.log(myValue == null);
  }

  someMethod() {
    let anotherNumber = 0;
    const aThirdNumber = 1;

    anotherNumber = 1;
    // anotherNumber = 'hello';

    console.log(this.myNumber);
    // this.myNumber = 8;
  }

  exploringArrays() {
    const myArray1 = new Array<number>();
    const myArray2 : number[] =[1,2,3,null];
    console.log(myArray1);
    console.log(myArray2);
    console.log(myArray2[1]);
    console.log(myArray2.splice(1,1));
    console.log(myArray2[1]);
    myArray2.push(4);
    myArray2.push(5);
    console.log(myArray2);
    console.log(myArray2.pop());
    console.log(myArray2);

    for (let i = 0; i < 10; i++) {
      console.log(i);
    }

    for (const next of myArray2) {
      console.log(next);
    }

    let num = 0;
    while (num < 5) {
      console.log(num);
      num++;
    }

    if (num < 5) {
      console.log('the number is less than 5');
    }
    else {
      console.log('the number is 5 or greater');
    }
  }
}
