import {Component, OnInit} from '@angular/core';
import {DataService} from '../../data.service';
import {ActivatedRoute, Router} from '@angular/router';
import {User} from '../../model/User';
import {FormResetService} from '../../form-reset.service';
import {AuthService} from '../../auth.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: Array<User>;
  selectedUser: User;
  action: string;
  loadingData = true;
  message = 'Please wait... getting the list of users';
  reloadAttempt = 0;
  isAdminUser = false;

  constructor(private dataService: DataService,
              private route: ActivatedRoute,
              private router: Router,
              private formResetService: FormResetService,
              private authService: AuthService) {
  }

  ngOnInit(): void {
    this.loadData();
    if (this.authService.role === 'ADMIN'){
      this.isAdminUser = true;
    }
    this.authService.roleSetEvent.subscribe(
      next => {
        if (next === 'ADMIN') {
          this.isAdminUser = true;
        }
        else {
          this.isAdminUser = false;
        }
      }
    );
  }

  loadData(): void {
    this.dataService.getUsers().subscribe(
      (next) => {
        this.users = next;
        this.loadingData = false;
        this.processUrlParams();
      },
      (error) => {
        if (error.status === 402) { // exemplary status sent from backend
          this.message = 'Sorry, you have to pay to use this app.';
        } else {
          this.reloadAttempt++;
          if (this.reloadAttempt < 10) {
            this.message = 'Sorry, something went wrong. Trying again... please wait.';
            this.loadData();
          } else {
            this.message = 'Sorry, something went wrong. Please contact support.';
          }
        }
      });
  }

  processUrlParams(): void {
    this.route.queryParams.subscribe(
      (params) => {
        const id = params['id'];
        this.action = params['action'];
        if (id) {
          this.selectedUser = this.users.find(user => user.id === +id);
        }
      });
  }

  setUser(id: number): void {
    this.router.navigate(['admin', 'users'], {queryParams: {id, action: 'view'}});
  }

  addUser(): void {
    this.selectedUser = new User();

    this.router.navigate(['admin', 'users'], {queryParams: {action: 'add'}});
    this.formResetService.resetUserFormEvent.emit(this.selectedUser);
  }

}
