import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {User} from '../../../model/User';
import {Router} from '@angular/router';
import {DataService} from '../../../data.service';
import {AuthService} from '../../../auth.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  @Input()
  user: User;

  @Output()
  dataChangeEvent = new EventEmitter();

  message = '';

  isAdminUser = false;

  constructor(private dataService: DataService,
              private router: Router,
              private authService: AuthService) {
  }

  ngOnInit(): void {
    if (this.authService.role === 'ADMIN'){
      this.isAdminUser = true;
    }
    this.authService.roleSetEvent.subscribe(
      next => {
        if (next === 'ADMIN') {
          this.isAdminUser = true;
        }
        else {
          this.isAdminUser = false;
        }
      }
    );
  }

  editUser(): void {
    this.router.navigate(['admin', 'users'], {queryParams: {action: 'edit', id: this.user.id}});
  }

  deleteUser(): void {
    const result = confirm('Are you sure you want to delete this user?');
    if (result) {
      this.message = 'deleting...';
      this.dataService.deleteUser(this.user.id).subscribe(
        next => {
          this.dataChangeEvent.emit();
          this.router.navigate(['admin', 'users']);
        },
        error => this.message = 'Sorry, this user cannot be deleted at this time.');
    }
  }

  resetPassword(): void {
    this.message = 'please wait...';
    this.dataService.resetUserPassword(this.user.id).subscribe(
      next => this.message = 'The passwordhas been reset.',
      error => this.message = 'Sorry, something went wrong.'
    );
  }

}
