import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../auth.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  userIsLoggedIn = false;

  constructor(private router: Router,
              private authService: AuthService) {
  }

  ngOnInit(): void {
    if (this.authService.isAuthenticated){
      this.userIsLoggedIn = true;
    }
  }

  navigateToRoomsAdmin(): void {
    this.router.navigate(['admin', 'rooms']);
  }

  navigateToUsersAdmin(): void {
    this.router.navigate(['admin', 'users']);
  }

  navigateToHome(): void {
    this.router.navigate(['']);
  }

  logout(): void {
    console.log('logout()');
    this.authService.logout();
    this.navigateToHome();
  }

}
